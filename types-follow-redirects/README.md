# Installation
> `npm install --save @types/follow-redirects`

# Summary
This package contains type definitions for follow-redirects (https://github.com/follow-redirects/follow-redirects).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/follow-redirects.

### Additional Details
 * Last updated: Mon, 14 Feb 2022 14:01:22 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Emily Klassen](https://github.com/forivall), [Claas Ahlrichs](https://github.com/claasahl), and [Piotr Błażejewicz](https://github.com/peterblazejewicz).
